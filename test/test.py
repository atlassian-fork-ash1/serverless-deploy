import os

import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


class DeployAWSServerlessDashboard(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service-dashboard')

    def tearDown(self):
        os.chdir('../..')

    @pytest.mark.skip(reason="This test requires an AdministratorAccess"
                      "permissions to be granted to an external account")
    def test_build_successfully_started_serverless_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'SERVERLESS_ACCESS_KEY': os.getenv('SERVERLESS_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')


class DeployAWS(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service')

    def tearDown(self):
        os.chdir('../..')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_successful_other_config(self):
        result = self.run_container(environment={
            'CONFIG': 'other-serverless.yml',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deplyment_fails_without_access_key_id(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'Deployment failed')

    def test_deplyment_fails_without_secret_access_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
        })

        self.assertRegexpMatches(
            result, r'Deployment failed')

    def test_extra_logs_present_if_debug(self):
        result = self.run_container(environment={
            'DEBUG': 'true',
        })

        self.assertRegexpMatches(
            result, r'Serverless: Load command config')

    def test_extra_args(self):
        result = self.run_container(environment={
            'EXTRA_ARGS': '--verbose --force',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'CloudFormation - UPDATE_IN_PROGRESS')

    def test_pipe_fails_wrong_config_path(self):
        invalid_path = 'no-such-yaml.yml'
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'CONFIG': invalid_path
        })

        self.assertRegexpMatches(
            result, rf'{invalid_path} doesn\'t exist')


class DeployGCP(PipeTestCase):

    def setUp(self):
        os.chdir('test/gcp-service')

    def tearDown(self):
        os.chdir('../..')
        if os.path.exists('test/gcp-service/gcp-keyfile.json'):
            os.remove('test/gcp-service/gcp-keyfile.json')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'GCP_KEY_FILE': os.getenv('KEY_FILE'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deplyment_fails_without_keyfile(self):
        result = self.run_container(environment={})

        self.assertRegexpMatches(
            result, r'Deployment failed')
